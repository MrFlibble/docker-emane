FROM       ubuntu:trusty
MAINTAINER Peter Boyd

RUN apt-get update && \
    apt-get install -y wget && \
    wget https://adjacentlink.com/downloads/emane/emane-1.2.1-release-1.ubuntu-14_04.amd64.tar.gz && \
	tar zxvf emane-1.2.1-release-1.ubuntu-14_04.amd64.tar.gz && \
	dpkg -i emane-1.2.1-release-1/debs/ubuntu-14_04/amd64/*.deb; apt-get install -f -y && \
	apt-get clean 
	
#	rm -r emane-1.2.1-release-1 emane-1.2.1-release-1.ubuntu-14_04.amd64.tar.gz && \
